# 1 - Translate Common Formulas Into Python Functions

Here you will find a list of common formulas used in fields such as mathematics, physics, chemistry and statistics. Convert the following mathematical expressions into 
functions written in Python so that all required variables are set as parameters of the functions and the final result is the returned value. 


- **Celsius to Fahrenheit** $`F = C \times \frac{9}{5} + 32`$ Test Values: With C = -50, Fahrenheit should be -58

- **Fahrenheit to Celsius** $`C = (F - 32) \times \frac{5}{9}`$ Test Values: With F = 32, Celsius should be 0

- For a given linear function: $`y = mx+c`$ **m** = **gradient (slope)** and **c** = **vertical intercept**
    - **Gradient between two points $`P_{1}(x_{1}, y_{1})`$ and $`P_{2}(x_{2}, y_{2})`$**: 
    
    ```math
    m = \frac{y_{2} - y_{1}}{x_{2} - x_{1}}
    ```
- **Volumetric Mass Density**: $`D = \frac{mass}{volume}`$

- **Percent Composition of an Element (n = the number of moles of the element in one mole of the compound, $`m_{e}`$ = molar mass of element, $`m_{c}`$ = molar mass of compound)**:

    ```math
    C = \frac{n \times m_{e}}{m_{c}} \times 100
    ```
    
- **Molarity (m)**: with m = moles of solute and l = liters of solution
    
    ```math
    M = \frac{m}{l}
    ```

- The  discharge  of  a  canal  (stream,  river)  is  the  quantity  or  volume  of  water  that  passes  per  unit  of  time. It is expressed in l(itres) per s(econd) = $`l/s`$ or in cubic metres per hour = $`m^3/h`$ or in cubic me-tres per $`day = m^3/day`$. If 1 $`l/s`$ is the same as $`\frac{3.6m^3}{s}`$. **Write a Python functions that converts from $`l/s`$ to  $`m^3/s`$**